#####################################
#####    Powershell Template    #####
#####     Created by Amir M     #####
#####################################

# Log file name #
$logfilename = ($PSScriptRoot + "\Logs\UserCreation_" + ((get-date).toString("dd-MM-yyyy")) + ".log")

# Load the JSON config file #
# To call a JSON entry, type $config.(ConfigTree).(Config) #
$configdir = $PSScriptRoot+'\Config.json'
$config = Get-Content $configdir | ConvertFrom-Json

# Load Window Formatting #
. '\\thezoo\uk\Software\Windows\Applications\Powershell Templates\Formatting\H15W80_BlackPlusCyan.ps1'

# Load Write-Log Function #
. '\\thezoo\uk\Software\Windows\Applications\Powershell Templates\Functions\LoggingFunction.ps1'

# Example of log writting #
#Write-Log -Level "INFO" -Message "Template Script Initiated." -logfile $logfilename

# Placeholder for formatting incase scripts #
Write-Host "Processing Script Parameters"
Clear-Host

Write-Host "TEST"
pause