## Include the following line to call the function in your script. Remove the prevailing '#' ##
#Invoke-Expression -command "'\\thezoo\uk\Software\Windows\Applications\Powershell Templates\Functions\LoggingFunction.ps1'"

## Logging example below. Remove the prevailing '#' ##
#$errorlogfilename = ("C:\ScriptDirectory\Logs\Error_" + ((get-date).toString("dd-MM-yyyy")) + ".log")
#Write-Log -Level "ERROR" -Message "Your script has produced an error" -logfile $errorlogfilename

Function Write-Log {
    [CmdletBinding()]
    Param(
    [Parameter(Mandatory=$False)]
    [ValidateSet("INFO","WARN","ERROR","FATAL","DEBUG")]
    [String]
    $Level = "INFO",

    [Parameter(Mandatory=$True)]
    [string]
    $Message,

    [Parameter(Mandatory=$False)]
    [string]
    $logfile
    )

    $Stamp = (Get-Date).toString("yyyy/MM/dd HH:mm:ss")
    $Line = "$Level $Stamp $Message"
    If($logfile) {
        Add-Content $logfile -Value $Line
    }
    Else {
        Write-Output $Line
    }
}