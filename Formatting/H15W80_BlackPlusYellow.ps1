##      Add the below line to your script to include this formatting. Remove the prevailing '#'       ##
# Invoke-Expression -command "powershell.exe -file '\\thezoo\uk\Software\Windows\Applications\Powershell Templates\PowershellFormatting.ps1'"

[CmdletBinding()]
Param(
     [Parameter(Mandatory=$False,Position=0)]
     [int]
     $Height = 15,
     [Parameter(Mandatory=$False,Position=1)]
     [int]
     $Width = 80
     )
$Console = $host.ui.rawui
$Buffer  = $Console.BufferSize
$ConSize = $Console.WindowSize

If ($Buffer.Width -gt $Width ) {
   $ConSize.Width = $Width
   $Console.WindowSize = $ConSize
}
$Buffer.Width = $Width
$ConSize.Width = $Width
$Buffer.Height = 3000
$Console.BufferSize = $Buffer
$ConSize = $Console.WindowSize
$ConSize.Width = $Width
$ConSize.Height = $Height
$Console.WindowSize = $ConSize

$Host.UI.RawUI.BackgroundColor = ($bckgrnd = 'Black')
$Host.UI.RawUI.ForegroundColor = 'Yellow'